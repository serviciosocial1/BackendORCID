const router = require('express').Router();
const persona = require('./persona');

router.get('/', (req, res) => {
    res.json({'info': 'Welcome to the gods API'})
});

router.use('/personas', persona);

module.exports = router;