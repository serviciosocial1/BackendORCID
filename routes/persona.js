const router = require("express").Router();
const { getPersonas, getPersona } = require("../controllers/persona");

router.get("/", getPersonas);
router.get('/:id', getPersona);

module.exports = router;
