const {Sequelize, DataTypes} = require('sequelize');
const sequelize = require("../config/db");

const Persona = sequelize.define('Persona', {
    nombre: {
        type:DataTypes.TEXT
    },
    orcid:{
        type:DataTypes.TEXT
    },
    depto:{
        type:DataTypes.TEXT
    },
    urlImagen:{
        type:DataTypes.TEXT
    },
},{timestamps: false});

module.exports = Persona;