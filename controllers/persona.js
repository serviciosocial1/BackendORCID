const Persona = require("../models/persona");

async function getPersonas(req, res) {
  const personas = await Persona.findAll();
  res.status(200).json(personas);
}
async function getPersona(req, res) {
  let id = req.params.id;
  const persona = await Persona.findByPk(id);
  res.status(200).json(persona);
}

module.exports = { getPersonas, getPersona };
