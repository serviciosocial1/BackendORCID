const sequelize = require('./config/db');
const express = require('express');
const app = express();
const PORT = 3000;
const routes = require('./routes');
const cors = require("cors")
app.use(cors())

app.use(express.json());
app.use('/', routes);

try {
    sequelize.authenticate();
    sequelize.sync();
    console.log('Connected to DB');
} catch (error) {
    console.log('Unable to connected to DB', error);
}

app.listen(PORT, () => {
    console.log(`Server listening on PORT ${PORT}`);
});